package com.testcric.demoquery.dto;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Persona {
    private String nombre;
    @Id
    private String identificacion;
    private Long resguardoId;

    public Persona() {
        this.nombre = nombre;
        this.identificacion = identificacion;
    }

    public Persona(String nombre, String identificacion) {
        this.nombre = nombre;
        this.identificacion = identificacion;
    }


    public Long getResguardoId() {
        return resguardoId;
    }

    public void setResguardoId(Long resguardoId) {
        this.resguardoId = resguardoId;
    }





    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }
}
