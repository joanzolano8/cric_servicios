package com.testcric.demoquery.controller;

import com.testcric.demoquery.dto.Persona;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController
{
    @GetMapping("/resguardo/{id}")
    public List<Persona> getPersonasPorResguardo(@RequestParam("id") Long idResguardo){
        List<Persona> personas = new ArrayList<>();
        personas.add(new Persona("Aldair Ramirez","987656789"));
        personas.add(new Persona("Joan Solano","123123489"));
        personas.add(new Persona("Carlos Solano","123456789"));
        return personas;
    }
}
